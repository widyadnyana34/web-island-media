<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Index</title>
	<style>
		*{
			margin: 0;
			padding: 0;
		}
		body{
			background: #eee;
			font-family: "Arial";
		}
		.padding-50{
			padding: 50px;
		}
		.days {
			padding: 5px 20px;
			background: #dcdde1;
			margin: 20px 0;
			display: flex;
			justify-content: center;
		}

		.day {
			padding: 8px 20px;
			background: #0097e6;
			margin: 15px;
			color: #f0f0f0;
			cursor: pointer;

		}
		.day.active{
			background: #44bd32;
		}
		.day.is-selected{
			border: 1px solid #FFF;
			box-shadow: 2px 3px 3px 2px rgb(145 145 145 / 60%);
		}
		.color-green{
			color: #44bd32;
		}
		.color-blue{
			color: #0097e6;
		}
	</style>
</head>
<body>
	<?php $days = array('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'); ?>
	<div class="padding-50">
		<center>
			<p>Today is <span class="color-green"><?= date('l') ?></span></p>
		</center>
		<div class="days">
			<?php foreach ($days as $day) { ?>
				<div class="day <?= date('l') == $day ? 'active' : '' ?>" onclick="return selectedThisDay(this)" day="<?= $day ?>">
					<?php echo $day; ?>
				</div>
				<?php
			} ?>
		</div>
		<center>
			<p>Selected day is <span id="show-selected" class="color-blue"></span></p>
		</center>
	</div>
	<script>
		function selectedThisDay(el)
		{
			let x = document.getElementsByClassName("is-selected");
			if(x.length > 0) { x[0].classList.remove("is-selected"); }

			el.classList.add("is-selected");

			var day = el.getAttribute("day");

			document.getElementById("show-selected").innerText = day;

		}
	</script>
</body>
</html>